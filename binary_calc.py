from sys import argv
script, number10 = argv

number10 = int(number10)

print

if number10 < 0:
    print "I don't know what to do with that yet.\n"
    exit(0)

print "Convert a number from base10 to binary."
print "You entered %d." % number10

number2 = []
number2_string = ''

while number10 > 1:
    if number10 % 2 == 0:
        number2.insert(0, 0)
        number10 = number10 / 2.0
    else:
        number2.insert(0, 1)
        number10 = number10 / 2.0
        number10 = number10 - 0.5

if number10 == 0:
    number2.insert(0, 0)
else:
    number2.insert(0, 1)

print "Your number in binary is:",
for i in number2:
    number2_string = number2_string + str(i)
print number2_string + "\n"

